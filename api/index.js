module.exports = (req, res) => res.send({ 
  message: `Hello, ${req.query.name || 'World'}, try changing ?name=xxx in query`
})
