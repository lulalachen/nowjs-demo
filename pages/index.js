export default () => (
  <div>
    Some react page <br />
    Visit <a href="/api?name=GuaGua">/api</a> to check out out API <br />
    Visit <a href="/api/sec">/api/sec</a> to check out out another API <br />
    Visit <a href="/api/third">/api/third</a> to check out out another API{' '}
    <br />
    Visit <a href="/second">/second</a> to check out out another Web page <br />
  </div>
);
